import numpy as np
import random
import matplotlib.pyplot as plt

random.seed(42)
np.random.seed(42)

a = 0.8
b = 20.

def f(x):
    return a*x + b

m = 100
X1 = np.random.randint(1000, size=m).astype(dtype=np.float32)
X2 = np.random.randint(1000, size=m).astype(dtype=np.float32)
Y = np.array([1 if x2 > f(x1) else 0
			for x1, x2 in zip(X1, X2)], dtype=np.float32)
for i in range(m):
	if random.random() < 0.03:
		Y[i] = 1 - Y[i] 



red = np.where(Y==1)
blue = np.where(Y==0)
plt.plot(X1[red], X2[red], 'ro')
plt.plot(X1[blue], X2[blue], 'bo')

plt.show()
